#!/usr/bin/perl -w

use strict ;

#foreach my $fa (<Halobacteriota_r*_final.ali>) {
foreach my $fa (<Alignments/*.ali>) {
	my $fn  = (split/\//,$fa)[-1] ;
	my $in  = (split/\./,$fn)[0] ;
	my $out = "$in".".trimali" ;
	my $cmd = "trimal -in $fa -out Trimmed/$out -automated1" ;
	print "EXECUTING: $cmd ...\n" ;
	system("$cmd") ;
}
