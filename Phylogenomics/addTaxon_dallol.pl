#!/usr/bin/perl
## Author: Laura Eme
#To add:
# give number to folders in order of creation
# coloring trees
# set possibility to opt out from every step
# make sure that sequences in genomic_datasets are on a single line
# add option to know if running on perun or bigboy
# add multithreaded blast and qsub option for perun
# store new sequences real headers somewhere
# blast all new seq against each other to flag paralog issues
###################################################################################

use lib '/local/one/SOFTWARE/bin/perllib';
use lauralib ;
use Bio::SeqIO::fasta;
use Bio::SearchIO;
use Bio::DB::Taxonomy;
use Scalar::Util qw(blessed);

$USAGE = "\n addTaxon.pl [parameter file]\n\n" ;

# Prints usage if no argument is given
unless( @ARGV )
{
	print $USAGE ;
	exit ;
}

@param = read_file($ARGV[0]);

print " ---------- Settings ---------- \n\n";
foreach $parameter (@param){
	chomp($parameter);
	($key,$value) = split (/:/,$parameter);
	$value = lc $value;
	$key = lc $key;
	$h_param{$key} = $value;
	print $key," : ",$value,"\n";
}

@taxaToAdd = read_file('TaxonToAddFile.txt');


#### Format genomic data file to make it blastable ####

unless (-e "./databases"){
	system("mkdir databases");
	print "Directory databases was created\n";
}

%h_dataType = ();
%h_shortName = ();
%h_matrix = ();

# Taxon list for concatenation
%h_allTaxa = ();
%h_expectedTaxa = ();

foreach $line (@taxaToAdd){
	chomp($line);
	@tab = split(/\t/,$line);
	$fastaFile = $tab[0];
	$NewTaxonName = $tab[1];
	$dataType = $tab[2];
	chomp($dataType);

	@tab = split(/\//,$fastaFile);
	$out = $tab[-1];
	$out = "./databases/".$out.".renamed";

	$h_dataType{$out} = $dataType;
	$h_shortName{$out} = $NewTaxonName;

	print  $NewTaxonName."\n" ;

	unless ( -e $out){
		$searchio = Bio::SeqIO->new(-format => 'fasta', -file   => $fastaFile);

		open(OUT, ">tmp")||die("writing error ...");

		while( my $seqobj = $searchio->next_seq ) {
			# name of the sequence
			$id = $seqobj->display_id();
			$id =~ s/\|/_/g;
			$desc = $seqobj->description();

			if ($desc eq ''){
				$new_line = $id;
			}else{
				$new_line = $id." ".$desc;
			}

			# let's remove any character that could
			$new_line =~ s/\"/_/g;
			$new_line =~ s/\'/_/g;
			$new_line =~ s/-/_/g;
			$new_line =~ s/;//g;
			$new_line =~ s/://g;
			$new_line =~ s/,//g;
			$new_line =~ s/\[//g;
			$new_line =~ s/\]//g;
			$new_line =~ s/\(//g;
			$new_line =~ s/\)//g;
			$new_line =~ s/\.//g;
			$new_line =~ s/ /_/g;
			$new_line =~ s/_+/_/g;
			$new_line =~ s/=/_/g;
			$new_line =~ s/\|/_/g;

			$sequence = $seqobj->seq();

			print OUT ">",$NewTaxonName."_".$new_line,"\n";
			print OUT $sequence,"\n";
		}
		close(OUT);

		$command = "mv tmp ".$out;
		system($command);
	}

	$test1 = $out.".nin";
	$test2 = $out.".pin";

	unless ((-e $test1) || (-e $test2)){
		if (($dataType eq 'prot') || ($dataType eq 'nucl')) {
			$command = "makeblastdb -in ".$out." -dbtype ".$dataType;
			system($command);
			print "MakeBlastBD was run on ".$fastaFile." and file was created in ./databases/\n";
		}else{
			print "Datatype not recognized for $line\n";
		}
	}
}

#### Get a seed for each dataset to update ####

$command = "./scripts/get_seeds2.pl";
system($command);

#### Copy datasets to be updated ####

unless (-e "updated_datasets"){
	system("mkdir updated_datasets");
}
system("cp initial_datasets/* updated_datasets/");

unless (-e "new_sequences"){
	system("mkdir new_sequences");
}

###### Run blast ######

open(MATRIX, ">matrix.OUT")||die;
print MATRIX "\t";

foreach $taxonDB ( keys(%h_dataType) ){
	$dataType = $h_dataType{$taxonDB};
	$shortName = $h_shortName{$taxonDB};
	print MATRIX $shortName, "\t";
}
print MATRIX "\n";

# Retrieve seed list
@seeds = <./seed_files/*>;

unless (-e "blast"){
	system("mkdir blast");
}

foreach $seed (@seeds){

	$fileToUpdate = $seed;
	$fileToUpdate =~ s/\.\/seed_files\///g;
	($fileToUpdate) = split(/\./,$fileToUpdate);
	$fileShortName = $fileToUpdate;
	print MATRIX $fileToUpdate, "\t";
	$fileToUpdate = "./updated_datasets/".$fileToUpdate."\.fas";

	print $seed,"\n";

	foreach $taxonDB ( keys(%h_dataType) ){

		$dataType = $h_dataType{$taxonDB};
		$shortName = $h_shortName{$taxonDB};
		$geneFound = 0;

		if ($dataType eq 'prot'){
			$command = "blastp -query ".$seed." -evalue 10e-05 -num_alignments ".$h_param{'nb_hits'}." -num_descriptions ".$h_param{'nb_hits'}." -out tmp.blastp -db ".$taxonDB;
			system($command);

			$blast_name = $fileShortName."vs".$shortName.".blastp";
			$command = "cp tmp.blastp blast/".$blast_name;
			system($command);

			#### Parsing BLAST output and retrieving protein sequence of the hit(s) #####
			my $in = new Bio::SearchIO(-format => 'blast',-file   => 'tmp.blastp');

			while( my $result = $in->next_result ) {
				$query_len = $result->query_length;
				$count_hits = 0;

				## $result is a Bio::Search::Result::ResultI compliant object
				while( my $hit = $result->next_hit ) {

					$count_hits++;

					## $hit is a Bio::Search::Hit::HitI compliant object
					while( my $hsp = $hit->next_hsp ) {

						$hit_name = $hit->name();
						print $hit_name,"\n";
						$hsp_len = $hsp->length(['total']);
						$ratio =  $hsp_len/$query_len;

						$percentid = $hsp->percent_identity();

						if (($ratio >= $h_param{'length_ratio'}) && ($percentid >= $h_param{'percentage_id'})){
							$command = "grep -A 1 \"".$hit_name."\" ".$taxonDB." > tmp.prot";
							system($command);

							@tmp = read_file('tmp.prot');
							$header = $tmp[0];
							if ($count_hits > 1){
								$shortName2 = $shortName."_".$count_hits;
								$header =~ s/>.*/>$shortName2/;

								# record old name and new name correspondance
								open(OUT, '>>', "ShortNamesMatch.txt")||die;
								print OUT $shortName,"_",$count_hits,"\t",$hit_name,"\n";
								close(OUT);
							}else
							{
								$header =~ s/>.*/>$shortName/;

								# record old name and new name correspondance
								open(OUT, '>>', "ShortNamesMatch.txt")||die;
								print OUT $shortName,"\t",$hit_name,"\n";
								close(OUT);
							}
							$sequence = $tmp[1];

							$tmp_prot = "new_sequences/".$fileShortName."-".$shortName."-".$count_hits.".prot";
							print $tmp_prot."\n";

							open(OUT, '>', $tmp_prot)||die;
							print OUT $header,$sequence;
							close(OUT);

							$command = "sed -i 's/\*//' ".$tmp_prot;
							system($command);

							##### Adding of the new sequence to the dataset

							$command = "sed -i -e \'\$a\\\' ".$fileToUpdate;
							system($command);
							$command = "cat ".$fileToUpdate." ".$tmp_prot." > tmp2.fas";
							system($command);
							$command = 'mv tmp2.fas '.$fileToUpdate;
							system($command);

							$geneFound = 1;


						}# end if (($ratio >= $ratio_cutoff) && ($percentid >= $ident_cutoff))
					}
				}
			}
		}
		else{
			$command = "tblastn -query ".$seed." -evalue 10e-05 -num_alignments ".$h_param{'nb_hits'}." -num_descriptions ".$h_param{'nb_hits'}." -out tmp.tblastn -num_threads 2 -db ".$taxonDB;
			print $command,"\n";
			system($command);

			###### Retrieving sequence portion that aligns with the query
			my $in = new Bio::SearchIO(-format => 'blast', -file   => 'tmp.tblastn');

			%h_seq = ();
			%h_start = ();
			%h_end = ();

			sub par_num { return $a <=> $b }

			while( my $result = $in->next_result ) {
				$query_name = $result->query_name;
				$query_len = $result->query_length;

				## $result is a Bio::Search::Result::ResultI compliant object
				while( my $hit = $result->next_hit ){

					@tab_start = ();

					$name = $hit->name() ;
					$desc = $hit->description() ;

					if ($desc eq ''){
						$seq_name = $name;
					}else{
						$seq_name =  $name." ".$desc;
					}

					## $hit is a Bio::Search::Hit::HitI compliant object
					while( my $hsp = $hit->next_hsp ){

						$num_hsp++;
						$hsp_len = $hsp->length(['total']);
						$ratio =  $hsp_len/$query_len;

						$percentid = $hsp->percent_identity();

						if($num_hsp == 1){
							if (($ratio >= $h_param{'length_ratio'}) && ($percentid >= $h_param{'percentage_id'})){

								$start = $hsp->start('subject');
								$end = $hsp->end('subject');
								$string = $hsp->hit_string();
								$eval = $hsp->evalue();

								push(@tab_start, $start);
								$h_seq{$start} = $string;
								$h_end{$start} = $end;
								$h_eval{$start} = $eval;
							}
							else{
								$next = 1;
							}
						}
						else{
							$start = $hsp->start('subject');
							$end = $hsp->end('subject');
							$string = $hsp->hit_string();
							$eval = $hsp->evalue();

							push(@tab_start, $start);
							$h_seq{$start} = $string;
							$h_end{$start} = $end;
							$h_eval{$start} = $eval;
						}
					}

					unless( $next == 1){

						@sort_tab_start = sort par_num @tab_start;

						$new_string = "";

						for $i (0 .. $#sort_tab_start ){

							$start = $sort_tab_start[$i];
							$string = $h_seq{$start};
							$eval = $h_eval{$start};
							$end = $h_end{$start};

							if ( $i == 0 ){
								$new_string = $string;
							}
							else{
								if ( $start < $prev_end ) {
									if( $end > $prev_end ) {

										$overlap = (($prev_end - $start)+1)/3;

										if ( $eval > $prev_eval ) {
											$tmp  = substr($string,$overlap);
											$string = $tmp;
										}
										else {
											$new_end = length($new_string) - $overlap;
											$new_string = substr($new_string,0,$new_end);
										}
									}
								} #end if ( $start < $prev_end )
								$new_string = $new_string.$string;
							}
							$prev_end = $end;
							$prev_eval = $eval;
						}

						$new_string =~ s/-//g;
						$new_string =~ s/\*//g;

						$tmp_prot = "new_sequences/".$shortName."-".$fileShortName.".prot";

						open(OUT,">$tmp_prot")||die;
						if ($count_hits > 1){
							print OUT ">".$shortName."_".$count_hits."\n";
						}else{
							print OUT ">".$shortName."\n";
						}
						print OUT $new_string."\n" ;
						close(OUT);

						##### Adding of the new sequence to the dataset

						$command = "sed -i -e \'\$a\\\' ".$fileToUpdate;
						system($command);
						$command = "cat ".$fileToUpdate." ".$tmp_prot." > tmp2.fas";
						system($command);
						$command = 'mv tmp2.fas '.$fileToUpdate;
						system($command);

						$geneFound = 1;
					}
				}
			}
		}

		print MATRIX $geneFound,"\t";
	}

	print MATRIX "\n";
}

###### alignments #####

@fastaToAlign = <updated_datasets/*fas>;

unless (-e 'linsi_files'){
	system('mkdir linsi_files');
}

unless (-e 'mafft_files'){
	system('mkdir mafft_files');
}

foreach $fasta (@fastaToAlign){
	$mafft = $fasta;
	$mafft =~ s/updated_datasets\///;
	($mafft) = split(/\./,$mafft);

	$mafft_file = "mafft_files/".$mafft.".mafft";
        unless (-e $mafft_file){
                $command = "mafft --anysymbol ".$fasta." > mafft_files/".$mafft.".mafft";
                system($command);
        }

#	$mafft_file = "linsi_files/".$mafft.".linsi";
#	unless (-e $mafft_file){
#		$command = "linsi --anysymbol ".$fasta." > linsi_files/".$mafft.".linsi";
#		system($command);
#	}
}

###### BMGE : Remove sites with >40% gaps
#@mafftFiles = <linsi_files/*>;
@mafftFiles =  <mafft_files/*>;

unless (-e 'bmge_files'){
	system('mkdir bmge_files');
}

#foreach $mafft (@mafftFiles){
#	$bmge = $mafft;
#	$bmge =~ s/linsi_files\///;
#	($bmge) = split(/\./,$bmge);

#	$bmge_file = "bmge_files/".$bmge.".bmge.phy";
#	unless (-e $bmge_file){

#		$command = "bmge -t AA -i ".$mafft." -m BLOSUM30 -g 0.4 -o bmge_files/".$bmge.".bmge.phy -of bmge_files/".$bmge.".bmge.fas";
#		system($command);
#	}
#}

foreach $mafft (@mafftFiles){
	$trimal = $mafft;
	$trimal =~ s/linsi_files\///;
	($trimal) = split(/\./,$trimal);

	$trimal_file = "trimal_files/".$trimal.".trimal.phy";
	unless (-e $trimal_file){

		$command = "trimal -automated1 -in ".$mafft." -out trimal_files/".$trimal.".trimal";
		system($command);
	}
}



###### Generate concatenated alignment #####


#$h_allTaxa{$NewTaxonName} = 1;

## call for fasta2cat.pl
if ( $h_param{'concatfile'} eq 'yes'){

	@trimalFiles = <trimal_files/*trimal>;

	# retrieve total taxa list

	foreach $trimalFile (@trimalFiles){
		$searchio = Bio::SeqIO->new(-format => 'fasta', -file   => $trimalFile);

		while( my $seqobj = $searchio->next_seq ) {
			$id = $seqobj->display_id();
			$h_allTaxa{$id} = 1;
		}
	}

	%h_taxon_seq = ();

	# Number and percentage of species present per gene
	%h_gene_stat = ();

	# Species present in each gene dataset
	%h_gene_which_sp = ();

	# Number and percentage of genes existing for each species
	%h_sp_nb_genes = ();

	# Number and percentage of sites existing for each species
	%h_sp_nb_sites = ();

	$num_taxa = keys %h_allTaxa;
	$num_fasta = $#trimalFiles+1;

	$count_k = 0;
	$count_j = 0;

	foreach $taxon (keys (%h_allTaxa)){
		$count_j++;
		$h_taxon_seq{$taxon} = "";

		foreach $trimalFile (@trimalFiles){

			$count_k = 0;
			$gene_name = $trimalFile;
			$gene_name =~ s/trimal_files\///;
			($gene_name) = split(/\./,$gene_name);

			$present = 0;

			$searchio = Bio::SeqIO->new(-format => 'fasta', -file => $trimalFile);
		LOOP_AA: while( my $seqobj = $searchio->next_seq ) {
			$count_k++;

			# name of the sequence
			$species = $seqobj->display_id();
			# sequence itself
			$sequence = $seqobj->seq();
			$len_seq = length($sequence);

			if ( ($count_j == 1) && ($count_k == 1) ){

				$total_len = $total_len + $len_seq;
				print " - ",$bmgeFile," - \n";
				print "Length ali: ".$len_seq."\n";
				print "Total ali length: ",$total_len,"\n";
			}

			if ( $taxon eq $species ){

				$h_taxon_seq{$species} = $h_taxon_seq{$species}.$sequence;
				$h_gene_stat{$gene_name}++;
				$h_sp_nb_genes{$species}++;
				$h_gene_which_sp{$gene_name} = $h_gene_which_sp{$gene_name}."\$".$taxon;

				$sequence_no_gap = $sequence;
				$sequence_no_gap =~ s/-//g;
				$len_seq_no_gap = length($sequence_no_gap);
				$h_sp_nb_sites{$taxon} = $h_sp_nb_sites{$taxon} + $len_seq_no_gap;
				#print $h_sp_nb_sites{$taxon}, "\n";

				$present = 1;
				last LOOP_AA;
			}
		}

			if ($present == 0){ ## IF the taxon is absent in this dataset, we generate an empty sequence of the same length
				$empty_seq = "";
				$empty_seq = '-'x$len_seq;
				$h_taxon_seq{$taxon} = $h_taxon_seq{$taxon}.$empty_seq;
			}
			#print "Taxon len: ".length($h_taxon_seq{$taxon})."\n\n";

		}
	}


	#### Creation of the concatenated sequence file #####

	unless (-e "concatenation"){
		system("mkdir concatenation");
	}

	$out = "concatenation/CAT_".$num_taxa."S".$num_fasta."F.fasta";

	open(OUT, ">$out")||die;

	foreach $taxon ( keys(%h_taxon_seq) ){
		print OUT ">".$taxon."\n";
		print OUT $h_taxon_seq{$taxon},"\n";
	}
	close(OUT);

	#### Creation of the log file #####

	$log = "concatenation/CAT_".$num_taxa."S".$num_fasta."F.log";
	open(OUT, ">$log")||die;
	print OUT "Gene\t";
	foreach $taxon (keys (%h_allTaxa)){
		print OUT $taxon, "\t";
	}
	print OUT "Taxa Number\tPerc. Taxa\n";

	foreach $gene (keys (%h_gene_which_sp)){

		$taxa_nb = 0;
		print OUT $gene, "\t";
		@tab = split(/\$/, $h_gene_which_sp{$gene});

		foreach $taxon (keys (%h_allTaxa)){
			$present = 0;

		LOOP_B:for $i (1..$#tab){
			$species = $tab[$i];

			if ( $species eq $taxon ){
				print OUT "1\t";
				$present = 1;
				$taxa_nb++;
				last LOOP_B;
			}
		}

			if ($present == 0){
				print OUT "0\t";
			}
		}

		print OUT $taxa_nb."\t";
		$perc = 100*($taxa_nb/$num_taxa);
		print OUT substr($perc, 0, index($perc,"\." )).substr($perc, index($perc,"\." ), 2)."\n";

	}

	print OUT "Number of genes\t";
	foreach $taxon (@taxa_list){
		print OUT $h_sp_nb_genes{$taxon},"\t";
	}
	print OUT "\n";

	print OUT "Perc. genes\t";
	foreach $taxon (@taxa_list){
		$perc = 100*($h_sp_nb_genes{$taxon}/$num_fasta);
		print OUT substr($perc, 0, index($perc,"\." )).substr($perc, index($perc,"\." ), 2),"\t";
	}
	print OUT "\n";


	print OUT "Number of sites\t";
	foreach $taxon (@taxa_list){
		print OUT $h_sp_nb_sites{$taxon},"\t";
	}
	print OUT  "\n";

	print OUT "Perc. Sites\t";
	foreach $taxon (@taxa_list){
		$perc = 100*($h_sp_nb_sites{$taxon}/$total_len);
		print OUT substr($perc, 0, index($perc,"\." )).substr($perc, index($perc,"\." ), 2),"\t";
	}
	print OUT "\n";
}



##### Tree reconstruction ####

if ( $h_param{'singlegenetree'} eq 'yes' ){

	print "*",$h_param{'software'},"*\n" ;

	@trimalFiles = <trimal_files/*phy>;

	foreach $trimal (@trimalFiles){

		$tree = $trimal;
		$tree =~ s/trimal_files\///;
		($tree) = split(/\./,$tree);

		if ( $h_param{'software'} eq 'fasttree'){

			unless (-e 'fasttree_files'){
				system('mkdir fasttree_files');
			}

			$command = "FastTree -lg -gamma ".$trimal." > fasttree_files/".$tree.".fasttree";
			system($command);
		}
		elsif ( $h_param{'software'} eq 'iqtree'){
			unless (-e 'iqtree_files'){
				system('mkdir iqtree_files');
			}

			$command = "iqtree-omp -omp 6 -s ".$trimal." -m LG4X -pre iqtree_files/".$tree;
			system($command);
		}
		elsif ( $h_param{'software'} eq 'raxml' ){
			print "RAxML step to be implemented! Go yell at Laura... \n\n"
		}
	}
}



######## Remove outliers #############
#
#for f in *fasttree ; do /home/laura/bin/sergio/rm_outliers_NonRecursive.pl $f ${f%%.*}'.homol.fas.nr' 0.99 1 ; done
#
#
#### Realign and trim ###
#
#for f in *NoOutliers.fasta ; do mafft-linsi $f > ${f%.*}'.mafft-linsi' ; bmge -t AA -i ${f%.*}'.mafft-linsi' -g 0.5 -of ${f%.*}'.mafft-linsi.g05' ; done
#
#
#for f in *mafft-linsi.g05 ; do  fasttree -gamma $f > $f'.fasttree' ; done
#
#for f in *g05..fasttree ; do mv $f ${f%.*}'fasttree' ; done
#
#
###### Coloring trees ######
#
#for f in *fasttree ; do tree2figtreev4.pl ~/bin/sergio/alveolates.col $f ; done
#
#
###### Get sequences from the coloured trees
#
#for f in *clean ; do figtree2ortho.pl ../${f%%.*}'.homol.fas.nr' $f ; done
#
#
###### Align #####
#
#for f in *selection ; do mafft-linsi $f  > ${f%%.*}'.mafft-linsi' ; done
#
##### BMGE default param
#
#for f in *linsi ; do bmge -t AA -i $f -g 0.2 -of ${f%.*}'.g02’ > ${f%.*}'.bmgeOUT' ; done
#
#
### Phylip
#
#for f in *02; do fasta2phylip.pl $f $f'.phy' ; done
#
#
###### Raxml
#
#for f in *phy; do echo 'raxmlHPC-PTHREADS-SSE3 -T 4 -m PROTGAMMALGF -s '$f' -n '${f%.*}' -p 1234 -N 5' > tmp ; cat main_perun_raxml tmp > rax_${f%%.*}".sh" ; done
#
#scp rax*sh leme@perun.biochem.dal.ca:/home/leme/sergio/nephromyces/11_raxml
#
#~/bin/submission.pl list_script
#
#
#### Coloring
#
#for f in *best* ; do tree2figtreev4.pl ~/bin/sergio/alveolates.col $f ; done
#
#
##### Keeping selected sequences
#
#for f in *nr; do echo $f ; figtree2ortho.pl $f RAxML_bestTree.${f%%.*}.g02.figtree.clean ; done
#
#
##### align ####
#
#for f in *fasta ; do mafft-linsi $f > ${f%.*}'.mafft-linsi' ; done
#
#
##### Trim ####
#
#for f in *mafft-linsi ; do bmge -t AA -i $f —g 0.9:0.2 -o ${f%.*}'.phy' >> BMGE.OUT ; done
#
#
###### Raxml
#
#for f in *phy; do echo 'raxmlHPC-PTHREADS-SSE3 -T 4 -m PROTGAMMALGF -s '$f' -n '${f%.*}' -p 1234 -N 20' > tmp ; cat main_perun_raxml tmp > rax_${f%%.*}".sh" ; done
#
#~/bin/submission.pl list_script
#
#chmod +x *sh
#
#
####### Get the taxa list_script
#
#grep ">" *nr | cut -d':' -f2 > list_taxa	# retrieve id names
#perl -pi -e "s/>//g" list_taxa			# remove ">"
#perl -pi -e "s/@.*//g" list_taxa		# remove accession numbers
#
#sort < list_taxa > foo
#uniq < foo > list_taxa				# remove duplicated taxa names
#
#
###### Move finished raxml
#
#for f in *best* ; do k=`echo $f | sed -s 's/.*\.//g'` ; j=` echo $k | sed -s 's/\..*//g'` ; mv *$j* done/ ; done
#
#
###### Keep the shortest branching sequence per species
#
#for f in *fasta ; do ~/bin/sergio/tree2OneSeqPerSpecies.pl RAxML_bestTree.${f%.*} $f list_taxa ; done
#
#
#
########### mafft ##############
#
#for f in *fasta ; do mafft-linsi $f > ${f%.*}'.mafft-linsi' ; done
#
#
##### Trim ####
#
#for f in *mafft-linsi ; do bmge -t AA -i $f —g 0.8:0.2 -o ${f%.*}'.phy' >> BMGE.OUT ; done
#
#
############## Generate concatenation #####################
#
#~/bin/sergio/fasta2cat.pl list_fasta list_taxa
#
#
#
#
#- run single gene trees with all 'clean' sequences (ie. there can be multiple sequence per strain/species)
#- run single gene trees with one sequence for each strain/species
#- run concatenation on all strains/species:
#-model?
#- run concatenation on selected strains/species
#-model?
#
#
