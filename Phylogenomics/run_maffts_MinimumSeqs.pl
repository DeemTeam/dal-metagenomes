#!/usr/bin/perl -w

use strict ;


#foreach my $fa (<GTDBtk_gene_markers/identify/intermediate_results/single_copy_fasta/ar53/*.fa>) {
foreach my $fa (<GTDBtk_gene_markers/identify/intermediate_results/single_copy_fasta/bac120/*.fa>) {
	#my $dir = (split/\//,$fa)[0] ;
	my $fn  = (split/\//,$fa)[-1] ;
	my $nsq = `grep -c \"\>\" $fa` ;
	if ($nsq) {
		chomp $nsq ;
		if ($nsq => 4) {
			$fn =~ s/\.fa// ;
			#my $ali = "$fn".".ali" ;
			my $ali = "$fn".".ali" ;
			my $cmd = "mafft --auto --thread 32 $fa > Alignments/$ali" ;
			print "EXECUTING $cmd ...\n" ;
			system("$cmd") ;
		} else {
			print "WARNING: skipping $fa because it has $nsq sequences\n" ; #<STDIN> ;
		}
	} else {
		print "WARNING: GREP RETURNED EMPTY\n" ; #<STDIN> ;
	}
}
