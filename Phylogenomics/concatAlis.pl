#!/usr/bin/perl -w

use strict ;

my $path = "/home/ana/LAB/Dallol/Metagenomes/MAGs/T1Sed10-126/GTDB_tree" ;

my (%genes, %spp) ;
my $genes = `ls $path/Trimmed` ;
if ($genes) {
	chomp $genes ;
	my @genes = (split/\n/,$genes) ;
	foreach my $file (@genes) {
		my $fn = (split/\//,$file)[-1] ;
		my $gn = (split/\./,$fn)[0] ;
		$genes{$gn} = 1 ;
	}
	
	my $heads = `grep ">" $path/Trimmed/* | cut -f2 -d ">" | sort | uniq` ;
	if ($heads) {
	chomp $heads ;
	my @heads = (split/\n/,$heads) ;
	foreach my $bug (@heads) {
		$spp{$bug} = 1 ;
	}
	
	my $cat = "DAL-T1Sed10-126_MAGs_GTDB_repsNmarkers_concatenated.trimali" ;
	#system("rm -v $path/$cat") ;		  
		
	my (%ali, %len) ; 

	my @rps ;

		# Read the fastas and make a matrix with the alignments

	foreach my $file (<$path/Trimmed/*.trimali>) { ## or change the way your files are named
		my $fn = (split/\//,$file)[-1] ;    ## Keeps the filename
		my $rp = (split/\./,$fn)[0]  ;     ## Keeps the KO. The variable is named rp because it was originally thought for ribosomal proteins :/
		my $j  = 0 ;                        ## initializes counter "j"
		next if ($file eq $cat) ;             ## Control not to read the final alignment in case you already have one.
	#	$rp =~ s/\.trimali//g ;             ## More cleaning on the ribosomal protein names
		print "Marker gene: $rp\n" ; #<STDIN> ;
		push @rps, $rp ;                    ## Adds the ribosomal protein name to a list (array)
		open (ALI, "$file") || die "Cant open $file\n" ;            ## opens each alignment
		$/ = "\>" ;                                                 ## Perl normally reads a file line by line. Here we change it to read sequence by sequence, or everything between the ">" of the fasta files
		<ALI> ;                                                     ## The first field will be empty, so we discard it
		while (<ALI>) {                                             ## Read seq by seq
			chomp ;                                                 ## get rid of the ">" character
			my @lines = (split/\n/)  ;                              ## split by lines
			my $sp    = shift @lines ; #print $head ; <STDIN> ;     ## The header is the first line, so we take it off from the array and store it in the head variable
			my $seq ;                
			foreach my $line (@lines) {
				$seq .= $line ;                                     ## We keep the sequence
			}
			my $len   = length($seq) ;                              ## We keep its length for a future control
	
#			print ">$head\n$seq\n" ; #<STDIN> ;
	
			$len{$rp} = $len ;
			$ali{$sp}{$rp} = $seq ;                                 ## We build a matrix in which the sequence will be contained in each cell, the name of the rows will be the species and the name of the columns the ribosomal proteins
		#	$spp{$sp} = 1 ;
			$j ++ ;                                                 ## we increase by one our counter "j" J contains the number of sequences
		}
		$/ = "\n" ;                                                 ## We give perl back its default value on how to read files
		print "$file ..... $j\n" ;
	}

	my ($i, $j, $k, $l) ;                                           ## these are counters

	open (CAT, ">$path/$cat") || die "Can't create $path/$cat\n" ;
	my $g = $#rps + 1 ;
		
	$i = $j = $k = $l = 0 ;             ## Set all the counters to zero
	foreach my $sp (sort keys %spp) {       ## Reads the species from the file
		print CAT ">$sp\n" ; #<STDIN> ;     ## Prints it as a header in the concat file
		print "$sp\t" ;  #<STDIN> ;
		
		$i = 1 ;                            ## Counter i set to 1 instead of zero
		
		foreach my $rp (sort @rps) {        ## now we are going to concatenate all the genes for each of the organisms
			my $len = $len{$rp} ;           ## Check the length of the gene
			$l ++ ;                         ## Counter l increases by one
			if ($ali{$sp}{$rp}) {           ## prints each gene after the other
				print CAT "$ali{$sp}{$rp}" ; #<STDIN> ;
				print "$rp\t" ;
			} else {
				foreach my $a (1..$len) {   ## in case that perticular species is missing that gene, we will make a cycle from one to the particular length of that gene
					print CAT "-" ;         ## and print "-" as many times as necessary
				}
				print "!$rp\t" ; #<STDIN> ;
			}
		}
		$k ++ ; # <STDIN> ;
		print CAT "\n" ;
		print "\n" ; #<STDIN> ;     ## and we're done, so we increase counter k by one and print a newline character in the concatenation file
		}
	}
}
