# DAL-metagenomes



## Extremely acidic proteomes and metabolic flexibility in bacteria and highly diversified archaea thriving in geothermal chaotropic brines

Codes for the article: **Extremely acidic proteomes and metabolic flexibility in bacteria and highly diversified archaea thriving in geothermal chaotropic brines**

_Ana Gutiérrez-Preciado, Bledina Dede, Brittany Baker, Laura Eme, David Moreira, Purificación López-García_

Ecologie Systématique Evolution, CNRS, Université Paris-Saclay, AgroParisTech, 91190 Gif-sur-Yvette, France

**Deem Team** @DEEMteam_Orsay


bioRxiv preprint: https://doi.org/10.1101/2024.03.10.584303
