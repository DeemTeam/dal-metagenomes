#!/usr/bin/perl -w

use strict ;

my $path = "/home/ana/LAB/Dallol/Metagenomes/MAGs/ArchaealMAGs/DAL/MetabolicG/Haloarchaea_MetabolicG_out" ;

my (@MAGs) ;
my (%Cats, %Matx) ;

my $hmfn = "METABOLIC_result_each_spreadsheet/METABOLIC_result_worksheet2.tsv" ;
open (HM, "$path/$hmfn") || die "Can't open $path/$hmfn\n" ;
my $head = <HM> ;
my @fields = (split/\t/,$head) ;
shift @fields ; shift @fields ; shift @fields ;
foreach my $f (@fields) {
	my $mag = (split/ /,$f)[0] ;
	$mag =~ s/\.Function\.presence// ;
	$mag =~ s/\_protein// ;
	$mag =~ s/GB\_// ;
	$mag =~ s/RS\_// ;
	push (@MAGs, $mag) ;
	print "$mag\n" ; #<STDIN> ;
}
	
	
while (<HM>) {
	chomp ;
	if ($_ =~ /Present/) {
	#	print "$_\n" ; <STDIN> ;
		my @fields = (split/\t/,$_) ;
		my $categ  = shift @fields  ;
		my $funct  = shift @fields  ;
		my $catfun = "$categ"."_"."$funct" ;
		$Cats{$catfun} = $1 ;
		shift @fields ;
		my $pos = 0 ;
		foreach my $f (@fields) {
			if ($f eq "Absent") {
			#	print "$categ, $funct $mag -> 0\n" ; <STDIN> ;
			} elsif ($f eq "Present") {
				my $mag = $MAGs[$pos]   ;
				$Matx{$mag}{$catfun} = 1 ;
			#	print "$categ, $funct... $mag -> 1\n" ; <STDIN> ;
			}
			$pos ++ ;
		}
	}
} close HM ;



my $out = "DAL-HaloGenomReps_MetabG_FunctionHit2.matrix" ;
open (OUT, ">$path/$out") || die "Can't create $path/$out\n" ;


## Build 0-1 matrix for both files:
my $header = "Category_Function\t" ;
foreach my $mag (@MAGs) {
	$header .= "$mag\t" ;
}
chop $header ;
print OUT "$header\n" ;


foreach my $fun (sort keys %Cats) {
	my $line = "$fun\t" ;
	foreach my $mag (@MAGs) {
		if ($Matx{$mag}{$fun}) {
			$line .= "1\t" ;
		} else {
			$line .= "0\t" ;
		}
	}
	chop $line ;
	print OUT "$line\n" ;
}




