#!/usr/bin/perl -w

use strict ;

my $path = "/home/ana/LAB/Dallol/Metagenomes/MAGs/ArchaealMAGs/DAL/MetabolicG/Haloarchaea_MetabolicG_out" ;

my (@MAGs) ;
my (%Cats, %Matx) ;

my $hmfn = "METABOLIC_result_each_spreadsheet/METABOLIC_result_worksheet4.tsv" ;
open (HM, "$path/$hmfn") || die "Can't open $path/$hmfn\n" ;
my $head = <HM> ;
my @fields = (split/\t/,$head) ;
shift @fields ; shift @fields ; shift @fields ; shift @fields ;
## Module step	## Module		## KO id		## Module Category
foreach my $f (@fields) {
	my $mag = (split/ /,$f)[0] ;
	$mag =~ s/ Module step presence// ;
	$mag =~ s/\_protein// ;
	$mag =~ s/GB\_// ;
	$mag =~ s/RS\_// ;
	push (@MAGs, $mag) ;
	print "$mag\n" ; #<STDIN> ;
}

my %total ;
while (<HM>) {
	chomp ;
#	if ($_ =~ /Present/) {
	#	print "$_\n" ; <STDIN> ;
		my @fields = (split/\t/,$_) ;
		my $modStp = shift @fields  ;
		my $module = shift @fields  ;
		my $KOid   = shift @fields  ;
		my $modCat = shift @fields  ;
	#	$Cats{$module} = 1 ;
		$total{$module} ++ ;
	#	$Cats{$modCat} = 1 ;
	#	shift @fields ;
		my $pos = 0 ;
		foreach my $f (@fields) {
			if ($f eq "Absent") {
			#	print "$categ, $funct $mag -> 0\n" ; <STDIN> ;
			} elsif ($f eq "Present") {
				$Cats{$module} = 1 ;
				my $mag = $MAGs[$pos]   ;
				$Matx{$mag}{$module} ++ ;
			#	print "$categ, $funct... $mag -> 1\n" ; <STDIN> ;
			}
			$pos ++ ;
		}
#	}
} close HM ;


my $out = "DAL-HaloarchaeaGenomReps_MetabG_KEGGradient.matrix" ;
open (OUT, ">$path/$out") || die "Can't create $path/$out\n" ;


my $header = "Module\t" ;
foreach my $mag (@MAGs) {
	$header .= "$mag\t" ;
}
chop $header ;
print OUT "$header\n" ;

foreach my $fun (sort keys %Cats) {
	my $line = "$fun\t" ;
	print "$fun has $total{$fun} steps\n" ; 
	foreach my $mag (@MAGs) {
		if ($Matx{$mag}{$fun}) {
			my $div = ($Matx{$mag}{$fun})/($total{$fun}) ;
			print "$mag has $Matx{$mag}{$fun} steps of $total{$fun} (so: $div)\n" ;# <STDIN> ;
			$line .= "$div\t" ;
		} else {
			$line .= "0\t" ;
		}
	}
	chop $line ;
	print OUT "$line\n" ;
}
