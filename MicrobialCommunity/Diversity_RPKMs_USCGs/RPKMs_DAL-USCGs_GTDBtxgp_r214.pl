#!/usr/bin/perl -w

use strict ;

my $path = "/home/ana/LAB/Dallol/Metagenomes/USCGs" ;
#my $path = "/mnt/users_home/ana/Metagenomes/Dallol/USCGs" ;
my (%names, %desc) ;
my $desc = "USCGs.desc" ; #$ARGV[1] ;

my $mgGp = "DAL" ;

open (DESC, "$path/$desc") || die "Can't open $path/$desc\n" ;
while (<DESC>) {
	chomp ;
	next if ($_ =~ /^\#/) ;
	my ($pfam, $name) = (split/\t/,$_)[0,1] ;
	$names{$pfam} = $name ;
	$desc{$name}  ++ ;
	print "$pfam, $name\n" ;
}
my $genes = (split/\./,$desc)[0] ;

my (%pmf, %metas) ;
my $tr = "$mgGp"."-totalReads.tab" ;
open (TR, "$path/$tr") || die "Can't open $path/$tr file. I need the total reads per sample to normalize by sequencing depth\n" ;
#<TR> ;
while (<TR>) {
	chomp ;
	my ($meta, $totReads) = (split/\t/,$_)[0,1] ;
	$metas{$meta} = 1 ;
#	$totReads	  =~ s/\,//g ;
	$pmf{$meta}   = $totReads/1000000 ; # This is my "per million" factor for each metagenome	
	print "$meta, $totReads:  $pmf{$meta}\n" ; #<STDIN> ;
} close TR ; #<STDIN> ;

my $orphans = "$genes"."_Without_MappedReads.tab" ;
open (ORPH, ">$orphans") || die "Can't create $orphans\n" ;

my %noTx ;
my $notaxa = "$genes"."_Without_Taxa.tab" ;
open (NOTX, ">$notaxa")  || die "Can't create $notaxa\n" ;


my (%mean, %name, %min, %max) ;
my (%rdCounts, %class, %rcid) ;
my (%GnsMg) ;
my $means = "$mgGp"."-"."$genes"."_meanlen_PFAM.tab" ; 
open (MEAN, "$means") || die "Can't open $means\n" ;
<MEAN> ;
while (<MEAN>) {
	chomp ;
	my ($cname, $pfam, $mean, $min, $max) = (split/\t/,$_) ;

#	next if ($pfam eq "PF13479") ;
#	next if ($pfam eq "PF06616") ;
		
	my $class = (split/\_/,$cname)[0] ;
	if ($class eq "NA") {
		$class = "NotAssigned" ;
	}
	
	my $cpfam = "$class"."_"."$pfam" ;
		
	$mean{$cpfam} = $mean ;
	$name{$cpfam} = $cname ;
		
	$min{$cpfam}  = $min  ;
	$max{$cpfam}  = $max  ;
		
#	print "$cname\t$cpfam\t$mean\n" ; <STDIN> ;
} close MEAN ;


my (%lens, %CPF, %genes) ;
#my (%rdCounts, %class, %rcid) ;
my $lens = "$mgGp"."-"."$genes"."_length_PFAM.tab" ;
	
open (LEN, ">$lens") || die "Can't create $lens\n" ;

#my $suf = "$code"."_"."$genes"."_TaxaGroup.tab" ;
foreach my $txgp (<$path/DAL-*_USCGs_GTDBreps_70c_35i_BH_GTDBtaxa.tab>) {
	my (%TX) ;
	my $fn = (split/\//,$txgp)[-1] ;
	my $mg = (split/\_USCGs/,$fn)[0]   ;
	#print "$mg\n" ; <STDIN> ;
	open (TX, "$txgp") || die "Can't open $txgp\n" ;
	while (<TX>) {
		chomp ;
		my ($id, $pfam, $group) = (split/\t/,$_)[0,1,3] ;
		next unless ($names{$pfam}) ;
		$TX{$id} = $group ;
	} close TX ;
		
	my $faa = "$path"."/"."$mg"."_"."$genes".".fna" ;
#	foreach my $faa (<USCGs/V*M*_$code*_USCGsMarkers.fna>) {
	$/ = "\>" ;
	open (FAA, "$faa") || die "Can't open $faa\n" ;
	<FAA> ;
	while (<FAA>) {
		chomp ;
		my ($header, $seq) = (split/\n/,$_)[0,1]   ;
		my ($id, $pfam) = (split/\|/,$header)[0,1] ;
		next unless ($names{$pfam}) ;
#		next unless ($TX{$id})      ;
			
#		my ($id, $cpfam)   = (split/\|/,$header)[0,1] ;
#		my $pfam = (split/\_/,$cpfam)[1] ;
#		next unless $names{$pfam} ;

		my $len = length $seq ;
		my ($class, $cpfam)  ;
			
		if ($TX{$id}) {
			$class = $TX{$id} ;
		} else {
			$class = "NotAssigned" ;
		}
			
		$cpfam = "$class"."_"."$pfam" ;
		
#		print     "$id\t$cpfam\t$len\n" ; <STDIN> ;
		print LEN "$id\t$cpfam\t$len\n" ; #<STDIN> ;
		
		$lens{$id}   = $len ;
		$genes{$id}  = $cpfam ;
		$CPF{$cpfam} = 1 ;
		$GnsMg{$id}  = $mg ;
	} close FAA ;
	$/ = "\n" ;
	
#	my (%rdCounts, %cov, %control, %seen, %class, %rcid) ;
	my (%control) ;
	my $mreadf = "MappedReads/"."$mg"."_USCGs.mapped.sam" ;
#	foreach my $mreadf (<MappedReads/$code*_USCGsMarkers.mapped.sam>) {
#	my $pmeta = (split/\_USCG/,$mreadf)[0] ;
#	my $meta  = (split/\//,$pmeta)[1] ;
	#print "$meta\n" ;  <STDIN> ;
	open (MAP, "$mreadf") || die "Can't open $mreadf\n" ;
	while (<MAP>) {
		chomp ;
		my ($read, $gene) = (split/\t/,$_)[0,2] ;
	#	print "$read\t$gene\t" ;  <STDIN> ;
	#	my ($cpfam, $name) = (split/\|/,$gene)[1,2] ;
		
		my ($id, $pfam) = (split/\|/,$gene)[0,1] ;
		my $cpfam = $genes{$id}   ;
		unless ($cpfam) {
			print "$id still without a C_Pfam\n" ; <STDIN> ;
		}
		next unless $names{$pfam} ;
#		print "$read\t$gene\t*$cpfam*\n" ;  #<STDIN> ;
		if ($CPF{$cpfam}) {
			
		#	$CPF{$cpfam} = 1 ;
			
			unless ($TX{$id}) {
				print "$id is not in TX hash ($cpfam)\n" ; #<STDIN> ;
				$noTx{$id}  = 1    ;
				$TX{$id}    = "NotAssigned" ;
			}
				
			$class{$TX{$id}} = 1 ;
				
			if ($control{$read}) {
#				print "$read already has a $control{$read}\nNOW: $cpfam\n" ; <STDIN> ;
				## I just count reads once
#				print "\n" ;
			} else {
				$control{$read} = $cpfam ;
			#	print "$read\t-->$id<--\t$cpfam\t$mg\n" ; <STDIN> ;
				
				$rdCounts{$mg}{$cpfam} ++  ;
				$rcid{$mg}{$id} ++ ;
					
#				print "$meta\t$cname\t$rdCounts{$meta}{$cname}\n" ; #<STDIN> ;
					
				## Change calcMeanLen script to sum those ribosomal genes that are the same but have different PFAMs
				## Calculate abundance (read coverage) and normalize by mean length for each gene
			}
		} else {
			print "\nWARNING: $cpfam is not in $means --> $id --- $pfam\n" ; 
			 <STDIN> ;
		#	These PFAMs will be excluded. This condition should not be met....
		}
	} close MAP ;
} ## foreach taxa group file

############ COVERAGE ##########

#my $sum  = "$code"."_"."$genes"."_Sum_RPKM_indv.tab" ;
#my $avg  = "$code"."_"."$genes"."_Avg_RPKM_indv.tab" ;
my $PFAM = "$mgGp"."-"."$genes"."_PFAM_RPKM_indv_r214.tab" ;
#open (AVG, ">$avg") || die "Can't create $avg\n" ;
#open (SUM, ">$sum") || die "Can't create $sum\n" ;
open (PF, ">$PFAM") || die "Can't create $PFAM\n" ;

my $header = "Gene\tPfam\tClass\t" ;
foreach my $meta (sort keys %metas) {
#	if ($meta =~ /Sal6/) {
#		$meta =~ s/Sal6/Sal06/ ;
#	}
	$header .= "$meta\t" ;
}
chop $header ;
print "$header\n" ;
print PF   "$header\n" ;
#print SUM  "$header\n" ;
#print AVG  "$header\n" ;


my %sum ;
	
	
my $all = "$mgGp"."-"."$genes"."_PFAM_RPKMperGene_r214.tab" ; 
open (ALL, ">$all") || die "Can't create $all\n" ;

foreach my $id (keys %genes) {
	
	my $cpfam = $genes{$id} ;
	my ($class, $pfam) = (split/\_/,$cpfam) ;
	print "$id\t$cpfam\n" ;
	
	
	unless ($pfam) {
		print "No PFAM? $cpfam ?" ; <STDIN> ;
	}
	

	my $name   = $name{$cpfam} ;
	
	
	my $meta = $GnsMg{$id} ;	
	
		#my $meta  = (split/\_\d{3}\d+/,$id)[0] ;
		#print "$meta -> META from ID $id\n" ; <STDIN> ;
		
		#	if ($class eq "d") {
		#	print "$meta\t $cpfam\n" ; <STDIN> ;
		#}

	if ($noTx{$id}) {
		print NOTX "$id\t$meta\t$names{$pfam}\t$class\n" ;
	}
	
	if ($rcid{$meta}{$id}) {
		print "RPKMs for each gene\n" ;
		my $readCounts = $rcid{$meta}{$id} ; print "Read Counts for $id: $readCounts\n" ;
		my $RPM        = $rcid{$meta}{$id} / $pmf{$meta} ; # Reads Per Million
													## Dividing the read counts by the “per million” 
													## scaling factor, normalizes for sequencing depth, 
													## giving you reads per million (RPM)
			## Divide the RPM values by the length of the gene, in kilobases. This gives you RPKM.
			#my $meanKB     = $mean{$name{$pfam}} / 1000 ; 
		print "Divided by the per million factor: $pmf{$meta} = $RPM\n" ;
		my $len        = $lens{$id} / 1000 ;
		my $RPKM       = $RPM / $len ;
		print "Divided by the length of the gene in Kb: $lens{$id} = $RPKM\n\n" ; #<STDIN> ;
#		print "len: $len\tRPKM: $RPKM\n" ; <STDIN> ;
	
	
	#	if ($names{$pfam}) {
	#		my $cdesc = "$class"."_"."$names{$pfam}" ;
	#		$sum{$meta}{$cdesc} += $RPKM ;
	#	print "SUM - $meta - $cdesc += $RPKM" ; <STDIN> ;
	#	} else {
	#		print "WARNING: $pfam! $desc\t$names{$pfam}*\n" ; <STDIN> ;
	#	}
		
		print "(line 261): $id\t$meta\t$names{$pfam}\t$class\t$RPKM\n" ; #<STDIN> ;
		print ALL  "$id\t$meta\t$names{$pfam}\t$pfam\t$class\t$RPKM\n" ;
	} else {
		print "WARNING: $id is empty in \"rcid\" hash! ($meta)\n" ; #<STDIN> ;
		print ORPH "$id\t$meta\t$names{$pfam}\t$class\n" ;
	}
}		
		
#foreach my $meta (sort keys %metas) {
#	print "$meta\n" ; <STDIN> ;
foreach my $cpfam (keys %CPF)   {
	
	my ($class, $pfam) = (split/\_/,$cpfam) ;
	#print "$class -- $pfam\n" ; <STDIN> ;
	
	my $name   = $name{$cpfam} ;
	
	my $rpkml = "$names{$pfam}\t$pfam\t$class\t"  ;
	
	foreach my $meta (sort keys %metas) {
		print "$meta\n" ; #<STDIN> ;	
		print "RPKMs for each Pfam\n" ;
		if ($rdCounts{$meta}{$cpfam}) {
		
			my $readCounts = $rdCounts{$meta}{$cpfam} ; 				print "Read Counts for $cpfam: $readCounts\n" ;
			my $RPM        = $rdCounts{$meta}{$cpfam} / $pmf{$meta} ;	print "Divided by the per million factor: $pmf{$meta} = $RPM\n" ;
			my $RPKM ;
			if ($mean{$cpfam}) {
			
				my $meanKB = $mean{$cpfam} / 1000 ;
				$RPKM      = $RPM / $meanKB ;							print "Divided by the mena length of the Pfams for that Taxa category: $meanKB = $RPKM\n" ; #<STDIN> ;
			
			} else {
				$RPKM = 0 ;												print "That Pfam was not present for that Taxa in the $meta metagenome, hence: RPKM = 0\n" ; <STDIN> ;
	
			}
			my $cdesc = "$class"."_"."$names{$pfam}" ;
			$rpkml .= "$RPKM\t" ;
		} else {
			$rpkml .= "0\t" ;
		}
		#	chop $rpkml ;
		#	print "**$rpkml**\n" ; <STDIN> ;
		#	print PF "$rpkml\n" ;
	}
	chop $rpkml ;
	print "**$rpkml**\n" ; # <STDIN> ;
	print PF "$rpkml\n" ;
}


