#!/usr/bin/perl -w

use strict ;
use warnings;

my $path = "/home/ana/LAB/Dallol/Metagenomes/USCGs" ;
#my $path = "/mnt/users_home/ana/Metagenomes/Dallol/USCGs" ;
my $desc = "USCGs.desc" ; 

open (DESC, "$desc") || die "Can't open $desc\n" ;
my $genes = (split/\./,$desc)[0] ;

my %names ;

while (<DESC>) {
	chomp ;
	next if ($_ =~ /^\#/) ;
	my ($pfam, $name) = (split/\t/,$_) ;
	$names{$pfam} = $name ;
} close DESC ;


my (%tot, %len, %min, %max, %txpf) ;

foreach my $txgp (<$path/DAL-*_USCGs_GTDBreps_70c_35i_BH_GTDBtaxa.tab>) {
	my (%TX) ;
	my $fn = (split/\//,$txgp)[-1] ;
	my $mg = (split/\_USCGs/,$fn)[0]    ;
	open (TX, "$txgp") || die "Can't open $txgp\n" ;
	while (<TX>) {
		chomp ;
		my ($id, $pfam, $group) = (split/\t/,$_)[0,1,3] ;
	#	my ($pid, $info) = (split/\t/,$_)[0,1]   ;
	#	my ($id,  $pfam) = (split/\|/,$pid)[0,2] ;
	#	my $group = (split/\|/,%info)[2] ;
		next unless ($names{$pfam}) ;
		$TX{$id} = $group ;
	} close TX ;
	my $fna = "$path"."/"."$mg"."_"."$genes".".fna" ;
	open (FNA, "$fna") || die "Can't open $fna\n" ;
	print "$mg\n" ; # <STDIN> ;
	$/ = "\n\>" ;
#	<FNA> ;
	while (<FNA>) {
		chomp ;
		my @lines = (split/\n/,$_) ;
		my $head  = shift @lines ;
		$head =~ s/\>//g ;
		
		my ($id, $pfam) = (split/\|/,$head)[0,1]  ;
		next unless ($names{$pfam}) ;
	
		my $seq ;
		foreach my $i (@lines) {
			$seq .= $i ;
		}
		
		my $l = length($seq) ;
		my ($class, $cpfam)  ;
		
		if ($TX{$id}) {
			$class = $TX{$id} ;
		} else {
			$class = "NA" ;
		}
			
		$cpfam = "$class"."_"."$pfam" ;
			
	#	print "$class\t$pfam\t$l\t$cpfam\n" ; <STDIN> ;
			
			
		$txpf{$cpfam} = 1 ;
			
		$len{$cpfam} += $l ;
		$tot{$cpfam} ++ ;	
			
		unless ($min{$cpfam}) {
			$min{$cpfam} = 1000000000000 ;
		}
		
		unless ($max{$cpfam}) {
			$max{$cpfam} = 0 ;
		}
		
		if ($l < $min{$cpfam}) {
			$min{$cpfam} = $l ;
	#		print "MIN $gene: $l\n" ; #<STDIN> ;
		}
			
		if ($l > $max{$cpfam}) {
			$max{$cpfam} = $l ;
	#		print "MAX $gene: $l\n" ; <STDIN> ;
		}
	}
	$/ = "\n" ;
}


my $out = "DAL-"."$genes"."_meanlen_PFAM.tab" ;
open (OUT, ">$out") || die "Can't create $out\n" ;

print     "Gene\tPFAM\tMean Length\tMin Length\tMax Length\n" ;
print OUT "Gene\tPFAM\tMean Length\tMin Length\tMax Length\n" ;



foreach my $cpfam (sort keys %txpf) {
	my $mean = ($len{$cpfam})/($tot{$cpfam}) ;
	my ($class, $pfam) = (split/\_/,$cpfam) ;
	my $name  = $names{$pfam} ;
	my $cname = "$class"."_"."$name" ;
	print OUT "$cname\t$pfam\t$mean\t$min{$cpfam}\t$max{$cpfam}\n" ;
	print     "$cname\t$pfam\t$mean\t$min{$cpfam}\t$max{$cpfam}\n" ; #<STDIN> ;
	#include names to sum different PFAMs into same gene
}
