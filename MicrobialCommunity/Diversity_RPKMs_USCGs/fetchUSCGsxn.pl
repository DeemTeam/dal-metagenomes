#!/usr/bin/perl -w

use strict ;

my $path  = "/home/ana/LAB/Dallol/Metagenomes/USCGs" ;
my $anno  = "/home/ana/LAB/Dallol/Metagenomes/Annotation" ;
my $uscgs = "USCGs.desc" ;
my %uscgs ;

open (USCG, "$path/$uscgs") || die "Can't open $path/$uscgs\n" ;
while (<USCG>) {
	chomp ;
	next if ($_ =~ /^\#/) ;
	my ($pfam, $desc) = (split/\t/,$_)[0,1] ;
	$uscgs{$pfam} = $desc ;
}

my $genes = (split/\./,$uscgs)[0]  ;
foreach my $hmm (<$path/DAL-*_Pfam-A.hmmblout>) {
	my $fn  = (split/\//,$hmm)[-1] ;
	my $mg  = (split/\_/,$fn)[0]   ;
	my $nuc = "$mg"."_USCGs.fna"   ;
	my $aa  = "$mg"."_USCGs.faa"   ;
	my $fna = "$anno"."/"."$mg"."_prokka"."/"."$mg".".ffn" ;
	my $faa = "$anno"."/"."$mg"."_prokka"."/"."$mg".".faa" ;
		
	print "Metagenome: $mg\nOutput file: $nuc\nNucleotide fna file: $fna\n\n" ; #<STDIN> ;
	
	open (HMM,  "$hmm") || die "Can't open $hmm\n"   ;
	open (NUC, ">$nuc") || die "Can't create $nuc\n" ;
	open (FNA,  "$fna") || die "Can't open $fna\n"   ;
	
	my (%ribos) ;
	while (<HMM>) {
	chomp ;
		next if ($_ =~ /^\#/) ;
		my ($id, $name, $pfv) = (split/ +/,$_)[0,2,3] ;
	#	print "$id\t$name\t$pfv\n" ; <STDIN> ;
		my $pfam = (split/\./,$pfv)[0] ;
		if ($uscgs{$pfam}) {
			$ribos{$id} = $pfam ;
		}
	} close HMM ;


	$/ = "\n\>" ;
	while (<FNA>) {
		chomp ;
		my @lines = (split/\n/,$_) ;
		my $head  = shift @lines   ;
		if ($head =~ /^\>/) {
			$head =~ s/\>// ;
		}
		
		my ($gene, $desc) ;
		
		my @words = (split/ /,$head) ;
		$gene = shift @words ;
		foreach my $word (@words) {
			$desc .= "$word " ;
		}
		chop $desc ;
	#	print "$gene\t$desc\n" ; #<STDIN> ;
		
		
		if ($ribos{$gene}) {
			my $seq ;
			foreach my $i (@lines) {
				$seq .= $i ;
			}
			print NUC ">$gene|$ribos{$gene}|$desc\n$seq\n" ; #<STDIN> ;
	#		print ">$gene|$ribos{$gene}|$desc\n$seq\n" ; <STDIN> ;
	#		print "$desc\n" ;
		} else {
	#		print "$gene no match in $fna\n" ; <STDIN> ;
		} 
	} close FNA ;
	
	open (OUT, ">$aa") || die "Can't create $aa\n" ;
	open (FAA, "$faa") || die "Can't open $faa\n"  ;
	
	$/ = "\n\>" ;
	while (<FAA>) {
		chomp ;
		my @lines = (split/\n/,$_) ;
		my $head  = shift @lines   ;
		if ($head =~ /^\>/) {
			$head =~ s/\>// ;
		}
		
		my ($gene, $desc) ;
		my @words = (split/ /,$head) ;
		$gene = shift @words ;
		foreach my $word (@words) {
			$desc .= "$word " ;
		}
		chop $desc ;
	#	print "$gene\t$desc\n" ; #<STDIN> ;
		
		if ($ribos{$gene}) {
			my $seq ;
			foreach my $i (@lines) {
				$seq .= $i ;
			}
			print OUT ">$gene|$ribos{$gene}|$desc\n$seq\n" ; #<STDIN> ;
			print ">$gene|$ribos{$gene}|$desc\n$seq\n" ; #<STDIN> ;
	#		print "$desc\n" ;
		} else {
	#		print "$gene no match in $fna\n" ;<STDIN> ;
		} 
	}
	
	
	$/ = "\n" ;	
}
