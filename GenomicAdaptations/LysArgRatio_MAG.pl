#!/usr/bin/perl -w

use strict ;

my $proteome = $ARGV[0] ;

my ($arg, $lys, $total) ;
open (PROT, "$proteome") || die "Can't open $proteome\n" ;
while (<PROT>) {
	chomp ;
	next if ($_ =~ /^\>/) ;
	my $seq = $_ ;
	$seq =~ s/\*//g ;
	my @aas = (split//,$seq) ;
	foreach my $aa (@aas) {
		$total ++ ;
		if ($aa eq "K") {
			$lys ++ ;
		} elsif ($aa eq "R") {
			$arg ++ ;
		}
	}
}

my $R = ($arg * 100) / $total ;
my $K = ($lys * 100) / $total ;
	
print "$arg ARG residues from a total of $total = $R %\n" ; 
print "$lys LYS residues from a total of $total = $K %\n\n" ;  # <STDIN> ;
	
my $ratio = $R/$K ;

print "R/K ratio: $ratio\n" ;
