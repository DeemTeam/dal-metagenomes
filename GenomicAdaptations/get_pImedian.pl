#!/usr/bin/perl -w

use strict ;

my (%iep, %MAGs) ;
my $n  = 0 ;
my $in = $ARGV[0] ;  ### Input is output from EMBOSS’ iep software

open (IN,  "$in")   || die "Can't open $in\n" ;
while (<IN>) {
	chomp ;
	if ($_ =~ /Isoelectric Point = (\d+\.\d+)/) {
		my $pI = $1 ;
		$n++ ;
		$iep{$n} = $pI ;
	}
} close IN ;
	
my $median;
my $mid = int $n/2 ; 
my @values ;
foreach my $i (1..$n) {
	push (@values, $iep{$i}) ;
}
my @sorted_values = sort by_number @values;
if (@values % 2) {
	   $median = $sorted_values[ $mid ];
} else {
	   $median = ($sorted_values[$mid-1] + $sorted_values[$mid])/2;
} 
print "$in\t$median\n" ;


sub by_number {
    if ($a < $b){ -1 } elsif ($a > $b) { 1 } else { 0 }
}
