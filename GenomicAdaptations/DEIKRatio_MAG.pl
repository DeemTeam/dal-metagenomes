#!/usr/bin/perl -w

use strict ;

my $proteome = $ARGV[0] ;

my ($de, $ik, $total) ;
open (PROT, "$proteome") || die "Can't open $proteome\n" ;
while (<PROT>) {
	chomp ;
	next if ($_ =~ /^\>/) ;
	my $seq = $_ ;
	$seq =~ s/\*//g ;
	my @aas = (split//,$seq) ;
	foreach my $aa (@aas) {
		$total ++ ;
		if ($aa eq "D") {
			$de ++ ;
		} elsif ($aa eq "E") {
			$de ++ ;
		} elsif ($aa eq "I") {
			$ik ++ ;
		} elsif ($aa eq "K") {
			$ik ++ ;
		}
	}
}

my $DE = ($de * 100) / $total ;
my $IK = ($ik * 100) / $total ;
	
print "$de D+E residues from a total of $total = $DE %\n" ; 
print "$ik I+K residues from a total of $total = $IK %\n\n" ;  # <STDIN> ;
	
my $ratio = $DE/$IK ;

print "D+E/I+K ratio: $ratio\n" ;
