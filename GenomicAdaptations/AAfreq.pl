#!/usr/bin/perl -w

use strict ;

my $dir = "/home/ana/LAB/Dallol/Metagenomes/Adaptations/AAfreq" ;

my $mat = "DAL_WC_AAfreq.matrix" ;						## The output file as a matrix with all the aa freqs from the metagenomes
open (MAT, ">$mat") || die "Can't create $mat\n" ;
my %mat ;												## This will be a hash of hashes where the matrix will be stored
my (%aas, %bugs) ;

foreach my $faa (<$dir/*.faa>) {						## We cycle through all the faa files in your directory
	my $fn  = (split/\.faa/,$faa)[0] ;					
	my $bug = (split/\//,$fn)[-1] ;						## We keep the name of each organism
	$bugs{$bug} = 1 ;
	print "Reading $bug...\n" ;
	my (@AAs, %AAs) ;									## Global genome counters. The array will contain all the Amino Acids together, the hash will contain the count of each AA and the AA will be its key
	open (FAA, "$faa") || die "Can't open $faa\n" ;		## open or die!
	while (<FAA>) {										## Cycle through each line of the faa file
		chomp ;											## Clean each line of newlines
		next if ($_ =~ /^\>/)    ;						## Skip those lines that begin with the crocodrile (i.e. skip the headers)
		my $cline = $_           ;
		$cline    =~ s/\*//g     ;						## Last time we had issues with some genomes that have an *, so we now clean each line to remove those
		$cline    =~ tr/a-z/A-Z/ ;						## Just being paranoid, translating all AAs to Capital Letters
		my @aas   = (split//,$cline) ;					## We split each line per character and put all aas in an array
		foreach my $aa (@aas) {							## We cycle through each aa of each line of the faa file
			next unless ($aa =~ /[A-Z]/)  ;				## Extra paranoid, if it's not a capital letter, we don't count it
			push (@AAs, $aa)  ;							## We push each aa to the global array for counting it later as our total
			$AAs{$aa} ++      ;							## We use the individual aa as a key to the global hash and point it to an increasing counter
		}
	}
	close FAA ;
	my $out = "$bug"."_AAfreq.tab" ;					## We create the output file for each genome
#	open (OUT, ">$out") || die "Can't create $out\n" ;
	my $total = scalar @AAs	;							## We get the total number of aas, the total elements of the global array
	print "$bug\t$total\n"  ;							## I print in the stodut the name of the genome and the total AAs, you can comment this line
	foreach my $aa (keys %AAs) {						## Cycle through each of the 20 aas
		my $freq  = $AAs{$aa}/$total  ;					## Divide the count of that aa in the genome by the total number of aas
		my $round = sprintf("%.4f", $freq) ;			## Print the frequency with only 2 decimals
	#	print "*$aa*\t$AAs{$aa}\t$total\t$freq\n"  ; #<STDIN> ; ## Control print that you can comment 
		print "*$aa*\t$AAs{$aa}\t$total\t$round\n" ; #<STDIN> ;
#		print OUT "$aa\t$round\n" ;						## Printing the AA and its frequency in the output file
		$mat{$bug}{$aa} = $round  ;
		$aas{$aa} = 1 ;
	} #<STDIN> ;
}

my $header = "Metagenome\t" ;
foreach my $aa (sort keys %aas) {
	$header .= "$aa\t" ;
}
chop $header ;
print MAT "$header\n" ;


foreach my $bug (keys %bugs) {
	my $line = "$bug\t" ;
	foreach my $aa (sort keys %aas) {
		if ($mat{$bug}{$aa}) {
			$line .= "$mat{$bug}{$aa}\t" ;
		} else {
			print "WARNING: $bug doesn't have $aa\n" ; <STDIN> ;
		}
	}
	chop $line ;
	print MAT "$line\n" ;
	print "$line\n" ; #<STDIN> ;
}



